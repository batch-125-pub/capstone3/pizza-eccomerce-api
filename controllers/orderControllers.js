
let Order = require('./../models/Orders');

let Product = require('./../models/Products');

let User = require('./../models/Users');

let auth = require('./../auth');

module.exports.addToCart = (user, prodId, quantity) => {
	let qty = parseInt(quantity);
	let price;
	let name;
	

	let product = Product.findById(prodId).then((product)=>{price = product.price; name = product.name; return product});



	return Order.findOne({userId: user.id, paymentStatus: false}).then((order, error)=>{
		if(error){
			return error;
		} else if (order) {
			
			let existingProduct = false; //default is set to false. will be set to true if product is already existing.

			order.products.forEach((prod)=>{ //checker if product is already existing in the products
				if(prod.productId == prodId){					
					existingProduct = true;
				}
			})

			//existingProduct == false;

			if(existingProduct == true){ //if product has been previously added already

		
				order.products.forEach((prod)=>{
					if(prod.productId == prodId){ //find the in the current order the product to be added.
						
						prod.quantity += qty; //increment quantity of the found product element
						prod.subtotal += (price * qty);
						prod.price = price;
						order.totalAmount += (price * qty);	
						order.cartCount += qty;	
					} 
				})

				return order.save().then((order, error) => { //saving the order
					if(error){
						return error; //product quantity was not incremented
					} else {
						return true;  //product quantity was successfully incremented
					}
				})

			} else { //if new product is added to the order
				
				order.totalAmount += (price * qty);
				order.cartCount += qty;
				order.products.push({productId: prodId, name: name, price: price, quantity: qty, subtotal: (price * qty)});

				return order.save().then((order, error) => {
					if(error){
						return error; //product and quanitity was not pushed or added
					} else {
						return true;  //product and the quantity was successfully pushed or added
					}
				})

			}
			
		} else { //if the user creates a new Order (no current order yet)

			// console.log("product price", price);

			return User.findOne({_id: user.id}).then((result)=> {

					let newOrder = new Order({
						totalAmount: (price * qty),
						userId: user.id,
						userFirstName: result.firstName,
						userLastName: result.lastName,
						products: [{
							productId: prodId,
							name: name,
							price: price,
							quantity: qty,
							subtotal: (price * qty)
						}],
						cartCount: qty
					})

					// console.log(newOrder);

					return newOrder.save().then((result, error)=>{
						if(error){
							return error;
						} else {
							return true; // will return true if the product was successfully saved;
						}
					})

			})		
		
		}
	})
}		

module.exports.currentOrder = (user) => {
	return Order.findOne({userId: user.id, paymentStatus: false}).then((order, error)=>{
		if(error){
			return error;
		} else if(order == null){
			return false; //will return false if user does not have a current order yet. frontend will display cart is empty;
		} else {
			return order; //will return order object if a current order is found.
		}
	})
}

module.exports.getOrderHistory = (user) => {
	return Order.find({userId: user.id, paymentStatus: true}).then((order, error)=> {
		if(error){
			return error;
		} else if (order == null){
			return false; //will return false if user does not have any order yet. Will display in front end: all order history will display here.
		} else {
			return order; //will return order objects of paid orders of the user
		}
	})
}

module.exports.getAllOrders = () => { //get orders of all users
	return Order.find({}).then((order)=>{return order});
}

module.exports.checkOut = (orderId) => {
	return Order.findByIdAndUpdate(orderId, {paymentStatus: true}, {new: true}).then((order, error)=>{
		// console.log("order in checkout", order);
		if(error){
			return error;
		} else {
			return true; //order successfully paid
		}
	})
}

