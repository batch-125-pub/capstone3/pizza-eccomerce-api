let express = require('express');

let mongoose = require('mongoose');

let cors = require('cors');

//const PORT = 3000;

const PORT = process.env.PORT || 3000;

let app = express();

//routes
let userRoutes = require('./routes/userRoutes');
let productRoutes = require('./routes/productRoutes');
let orderRoutes = require('./routes/orderRoutes');

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


mongoose.connect("mongodb+srv://sarahdidulo:admin@cluster0.lxfuk.mongodb.net/The_Pizza_District?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
).then(()=>{
	console.log(`Succesfully Connected to Database`);
}).catch((error)=>{
	console.log(error);
})


//app routes
app.use('/api/users/', userRoutes);
app.use('/api/products/', productRoutes);
app.use('/api/orders/', orderRoutes);


app.listen(PORT, () => {
	console.log(`Server is now connected to ${PORT}`);
})