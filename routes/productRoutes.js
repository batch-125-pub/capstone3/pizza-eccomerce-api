let express = require('express');

let router = express.Router();

let productController = require('./../controllers/productControllers');

let auth = require('./../auth');

//console.log("in productRoutes");

router.post('/create-product', auth.verify, (req, res)=> { //add auth.verify whe1n frontend is set up
	//console.log("in create product route");
	productController.createProduct(req.body).then(result=>res.send(result));
})

//in getting all products, add auth.verify and decode (to check if admin), to know if the user is an admin, and return boolean to front end if the user is an admin or not -> return will determine the displayed buttons or functionalities for admin and non-admin

//admin-> will display Create Product btn, Edit Product btn, Delete Product btn, Archive Product btn (note: Admins cannot create orders or add to cart), retrieve single product? (ask miss)

//non-admin -> will display only add to cart(creating an order), quantity (number field), retrieve single product? (ask miss)

//retrieve active products
router.get('/', (req, res)=> {
	productController.getAllActiveProd().then(result=>res.send(result));
})

//retrieve active and inactive products
router.get('/all-products', auth.verify, (req, res)=> {
	productController.getAllProducts().then(result=>res.send(result));
})

//view a single product
router.get('/:productId', (req, res)=> {
	productController.getSingleProduct(req.params.productId).then(result=>res.send(result));
})

router.put('/:productId/edit', auth.verify, (req, res)=> {
	productController.editProduct(req.params.productId, req.body).then(result=>res.send(result));
})

router.delete('/:productId/delete', auth.verify, (req, res)=>{
	productController.deleteProduct(req.params.productId).then(result=>res.send(result));	
})

router.put('/:productId/archive', auth.verify, (req, res)=> {
	productController.archive(req.params.productId).then(result=>res.send(result));
})

router.put('/:productId/unarchive', auth.verify, (req, res) => {
	productController.unarchive(req.params.productId).then(result=>res.send(result));
})



module.exports = router;