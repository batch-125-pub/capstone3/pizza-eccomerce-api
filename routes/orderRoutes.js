let express = require('express');

let router = express.Router();

let orderController = require('./../controllers/orderControllers');

let auth = require('./../auth');

router.post('/:productId/add-to-cart', auth.verify, (req, res)=> {
	let current_user = auth.decode(req.headers.authorization); //user id
	//let qty = parseInt(req.body.quantity);
	orderController.addToCart(current_user, req.params.productId, req.body.quantity).then(result=>{res.send(result);});
	//change to authUser = req.headers.authorization when frontend is made;//place 'Bearer token' in postman JSON to test
	//let current_user = auth.decode(req.headers.authorization);
	//console.log("current user", current_user);
	//res.send(current_user);
	//console.log("quantity: ", req.body.quantity);
	//orderController.addToCart(current_user, req.params.productId, req.body.quantity).then(result=>res.send(result));
})

router.get('/current-order', auth.verify, (req, res)=> {
	let current_user = auth.decode(req.headers.authorization); //user id
	//let qty = parseInt(req.body.quantity);
	orderController.currentOrder(current_user).then(result=>res.send(result));
	//change to authUser = req.headers.authorization when frontend is made;//place 'Bearer token' in postman JSON to test
	//let current_user = auth.decode(req.headers.authorization);
	//console.log("current user", current_user);
	//res.send(current_user);
	//console.log("quantity: ", req.body.quantity);
	//orderController.addToCart(current_user, req.params.productId, req.body.quantity).then(result=>res.send(result));
})

///:productId/add-to-cart


router.put('/:orderId/checkout', auth.verify, (req, res)=>{
	//let current_user = auth.decode(req.headers.authorization);
	orderController.checkOut(req.params.orderId).then(result=>res.send(result));
})

router.get('/order-history', auth.verify, (req, res)=>{
	let current_user = auth.decode(req.headers.authorization);
	orderController.getOrderHistory(current_user).then(result=>res.send(result));
})

router.get('/all-orders', auth.verify, (req, res)=>{
	orderController.getAllOrders().then(result=>res.send(result));
})

module.exports = router;