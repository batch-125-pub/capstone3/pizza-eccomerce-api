let express = require('express');

let router = express.Router();

let userController = require("./../controllers/userControllers");

let auth = require('./../auth');

router.post("/check-email", (req, res) => {
	userController.checkEmail(req.body).then(result=>res.send(result));
})

router.post("/register", (req, res) => {
	userController.register(req.body).then(result=>res.send(result));
})

router.post("/login", (req, res)=> {
	//console.log(req.headers.authorization);
	userController.login(req.body).then(result=>{res.send(result)});

	//res.send(result))
})


router.get("/get-profile", auth.verify, (req, res)=> { //add auth.verify later. //change to GET when frontend or fetch is made
	//console.log(req.body.token);
	//req.headers.authorization
	let current_user = auth.decode(req.headers.authorization);
	userController.getProfile(current_user).then(result=>res.send(result));
	//userController.getProfile(req.body.token).then(result=>res.send(result));
	//let current_user = auth.decode(req.headers.authorization); //catching from frontend?
	//console.log("decoded user: ", current_user);
	//userController.getProfile(); // create getProfile if needed
})

router.get("/get-all-users", auth.verify, (req, res)=>{
	userController.getAllUsers().then(result=>res.send(result));
})

router.put("/:userId/change-admin-status", auth.verify, (req, res)=>{
	userController.setToAdmin(req.params.userId).then(result=>res.send(result));
})

module.exports = router;